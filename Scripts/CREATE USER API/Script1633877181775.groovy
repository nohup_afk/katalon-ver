import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.TestObjectProperty
import com.kms.katalon.core.testobject.impl.HttpTextBodyContent
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS

String endpoint = "https://reqres.in/api/users/2"
String requestMethod = "PUT"
String authHeader = ""
 
TestObjectProperty header1 = new TestObjectProperty("Authorization", ConditionType.EQUALS, authHeader)
TestObjectProperty header2 = new TestObjectProperty("Content-Type", ConditionType.EQUALS, "application/json")
TestObjectProperty header3 = new TestObjectProperty("Accept", ConditionType.EQUALS, "application/json")
ArrayList defaultHeaders = Arrays.asList(header1, header2, header3)
 
String body = '{"name": "morpheus", "job": "bad resident"}'


	RequestObject ro = new RequestObject("objectId")
	ro.setRestUrl(endpoint)
	ro.setHttpHeaderProperties(defaultHeaders)
	ro.setRestRequestMethod(requestMethod)
	ro.setBodyContent(new HttpTextBodyContent(body))
	 
	ResponseObject respObj = WS.sendRequest(ro)
	
	
	if(SampleResponseObject.getStatusCode(respObj) != 200) {
		KeywordUtil.markFailed("Status code is not 201 as expected. It is "
		+ SampleResponseObject.getStatusCode(respObj))
		}
		 
		if(SampleResponseObject.getWaitingTime(respObj) > 5000) {
		KeywordUtil.markFailed("Your request takes more than 5000ms. Actual time is "
		+ SampleResponseObject.getWaitingTime(respObj))
		}
