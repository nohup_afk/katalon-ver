import org.junit.runner.RunWith;

import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;



CucumberKW.runFeatureFolderWithTags('Include/features/kata_ai_test_cases.feature',"@tag","@automationPactice1")

@RunWith(Cucumber.class)
@CucumberOptions(features="Include", glue="", plugin = ["pretty",
					"junit:Include/cucumber.xml",
					"html:Include",
					"json:Include/cucumber.json"])
public class MyCucumberRunner {
}