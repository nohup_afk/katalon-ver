import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords;

import com.kms.katalon.core.webui.driver.DriverFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class automation {
	
	public WebDriver driver ;
	public WebDriverWait wait;
	public JavascriptExecutor js ;
	
	@Given("I navigate to automationpractice.com")
	public void i_navigate_to_automationpractice(){
		
		WebUiBuiltInKeywords.openBrowser("http://automationpractice.com");
		driver = DriverFactory.getWebDriver();
		wait = new WebDriverWait(driver,30);
		js = (JavascriptExecutor) driver;
	}
	@Given("i click signIn")
	public void  i_click_signIn() {
		driver.findElement(By.xpath("//*[@href = 'http://automationpractice.com/index.php?controller=my-account']")).click();	
	}
	@Given("I input email address (.*)")
	public void I_input_email_address(String email) {
		wait.until(ExpectedConditions.elementToBeClickable(By.id("email_create")));
		WebElement email_create = driver.findElement(By.id("email_create"));
		email_create.clear();
		email_create.sendKeys(email);	
		
	}
	
	@When("I click button create account")
	public void I_click_button_create_account() {
		 driver.findElement(By.id("SubmitCreate")).click();
	}
	
	@And("input my title (.*)")
	public void input_my_title (String titel)  {
		WebUiBuiltInKeywords.delay(3);
		if ( titel.equalsIgnoreCase("mr")) {
			driver.findElement(By.xpath("//input[@id = 'id_gender1']")).click();
		} else if ( titel.equalsIgnoreCase("mrs")) { 
			driver.findElement(By.id("uniform-id_gender2")).click();
		}
	}
	
	@And("input firstname (.*) and lastname (.*)")
	public void input_fname_lname(String fname, String lname) {
		driver.findElement(By.id("customer_firstname")).sendKeys(fname);
		driver.findElement(By.id("customer_lastname")).sendKeys(lname);
	}
	
	@And("input password account (.*)")
	public void input_password_account(String pass) {
		
		WebElement element = driver.findElement(By.id("passwd"));
		js.executeScript("arguments[0].scrollIntoView(true);", element);
		element.sendKeys(pass); 
	}
	@And("input select my birthdate date (.*)  month (.*) and year (.*)")
	public void selectdatemont(String date,String month,String year) {
		
		
		int number = Integer.parseInt(date);
		int nMonth = Integer.parseInt(month);
		int nyears = Integer.parseInt(year);
		
		
		
		WebElement elDays = driver.findElement(By.id("days"));
		elDays.click();
		Select datebirth = new Select(driver.findElement(By.cssSelector("#days")));
		datebirth.selectByIndex(number);
		
		WebElement elMonth = driver.findElement(By.id("months"));
		elMonth.click();
		Select monthbirth = new Select(elMonth);
		monthbirth.selectByIndex(nMonth);
		
		WebElement elYears = driver.findElement(By.id("years"));
		elYears.click();
		Select ybirth = new Select(elYears);
		ybirth.selectByIndex(nMonth);	
	}
	
	@And("input credentials myaddres (.*) lname (.*)")
	public void input_credentials_myaddres(String fname,String lname) {
		WebElement element = driver.findElement(By.id("firstname"));
		js.executeScript("arguments[0].scrollIntoView(true);", element);
		
		element.sendKeys(fname);
		
		driver.findElement(By.id("lastname")).sendKeys(lname);
		
	}
	
	@And("input company (.*) addr (.*)")
	public void input_company_addr(String cmp,String addr) {
		
		driver.findElement(By.id("company")).sendKeys(cmp);
		
		WebElement element = driver.findElement(By.id("address1"));
		
	
		js.executeScript("arguments[0].scrollIntoView(true);", element);
		
		element.sendKeys(addr);
	}
	
	@And("input city (.*) state (.*)")
	public void input_city_state(String city, String state) {
		WebElement element = driver.findElement(By.id("city"));
				
		js.executeScript("arguments[0].scrollIntoView(true);", element);
		
		element.sendKeys(city);
		
		Select datebirth = new Select(driver.findElement(By.id("id_state")));
		datebirth.selectByVisibleText(state);
		
		
	}
	
	@And("input  postal (.*) phone (.*)")
	public void input_postal_phone(String postal,String phone) {
		WebElement element = driver.findElement(By.id("postcode"));
		
		js.executeScript("arguments[0].scrollIntoView(true);", element);
		
		element.sendKeys(postal);
		
		driver.findElement(By.id("phone_mobile")).sendKeys(phone);
		
		
	}
	
	@Then("I able register successfuly")
	public void I_able_register_successfuly() {
		driver.findElement(By.id("submitAccount")).click();
	}
	
	
	@Given("I input email (.*)")
	public void I_input_email(String email) {
		driver.findElement(By.id("email")).sendKeys(email);
	}
	
	@Given("I input password login (.*)")
	public void i_password(String pass) {
		driver.findElement(By.id("passwd")).sendKeys(pass);
	}
	
	@When("I click button SignIn")
	public void I_click_button_SignIn() {
		driver.findElement(By.id("SubmitLogin")).click();
		
	}
	
	
	@Then("I should be able to login successfully")
	public void I_should_be_able() {
		String heading = driver.findElement(By.xpath("//h1[@class = 'page-heading']")).getText();
		assertEquals("MY ACCOUNT",heading);
	}
	
	
	
	
	
	

}