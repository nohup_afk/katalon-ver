#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Kata.ai registration and Login
  As a user, I want to registration to platform.kata.ai
  so that I can make an login to kata ai

  @RegistrationUser
  Scenario Outline:  Test case registration with a valid credential
    Given I navigate to kata.ai platform
    * I click Sign Up
    * I input username <username> and email <email>
    * I Select my role <role>
    When I click captcha
    Then I success registration <status>

    Examples: 
      | username | email           | status  |
      | Aziz     | email@email.com | success |

  @LoginUser
  Scenario Outline: test case login with a valid credential
    Given I navigate to kata.ai platform
    * I input username <username>
    * I input password <password>
    When I click captcha
    Then I should be able to login successfully

    Examples: 
      | username | password    |
      | Aziz     | password!!! |

  @automationPactice1
  Scenario Outline: login with a valid credential
    Given I navigate to automationpractice.com
    * i click signIn
    * I input email <email>
    * I input password login <password>
    When I click button SignIn
    Then I should be able to login successfully

    Examples: 
      | email                  | password           |
      | fishrotten00@gmail.com | thispasswordisweak |

  @automationPactice2
  Scenario Outline: registration with a valid credential
    Given I navigate to automationpractice.com
    * i click signIn
    * I input email address <email>
    When I click button create account
    And input my title <title>
    And input firstname <fname> and lastname <lname>
    And input password account <password>
    And input select my birthdate date <date>  month <month> and year <year>
    And input credentials myaddres <fname> lname <lname>
    And input company <company> addr <address>
    And input city <city> state <state>
    And input  postal <postal> phone <phone>
    Then I able register successfuly

    Examples: 
      | email                  | title | fname | lname | password           | date | month | year | company               | address     | city | state  | postal | phone        |  |
      | fishrotten00@gmail.com | mr    | AAA   | AAA   | thispasswordisweak |   11 |    11 | 1990 | battefield badcompany | addressbois | Aka  | Alaska |  25000 | 081231233123 |  |
